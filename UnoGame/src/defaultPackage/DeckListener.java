package defaultPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * DeckListener class to provide button functionality
 * 
 * @author Rahul and Rishabh
 * @version May 8, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: none
 */
public class DeckListener implements ActionListener {
	UnoPlayer player;

	UnoDisplay display;

	/**
	 * Constructor for DeckListener
	 * 
	 * @param g
	 *            The Uno game
	 * @param d
	 *            The display for the Uno game
	 */
	public DeckListener(UnoPlayer p, UnoDisplay d) {
		player = p;
		display = d;
	}

	/**
	 * Draws the top card when the player clicks on the deck.
	 * 
	 * @param e
	 *            is the ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (player.getGame().getCurrentPlayer() == player && !player.canPlay()) {
			if (player.getGame().getDeck().isEmpty())
				player.getGame().flipDiscardToDeck();
			player.addCard(player.getGame().drawCard());
			player.getGame().nextPlayer();
			display.refreshAll(true);
		}
	}

}
