package defaultPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * DifficultyListener class to provide button functionality for selecting
 * difficulty of the AI colors
 * 
 * @author Rahul and Rishabh
 * @version May 8, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: none
 */
public class DifficultyListener implements ActionListener {

	private JButton choice; // The Button Selected

	private UnoGame game; // Current UnoGame

	private UnoDisplay display; // Current UnoDisplay

	/**
	 * @param button
	 *            the selected JButton for either easy or hard
	 * @param g
	 *            the current UnoGame
	 * @param d
	 *            the current UnoDisplay
	 */
	public DifficultyListener(JButton button, UnoGame g, UnoDisplay d) {
		choice = button;
		game = g;
		display = d;
	}

	/**
	 * Sets the difficulty of the AI based on the player's selection on the menu
	 * 
	 * @param arg0
	 *            is the ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (choice.getText().equalsIgnoreCase("easy")) {
			game.setAI(false);
		} else {
			game.setAI(true);
		}
		display.refreshAll(true);
	}

}
