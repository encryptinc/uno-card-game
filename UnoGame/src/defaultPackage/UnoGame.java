package defaultPackage;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import javax.imageio.ImageIO;

/**
 * Class representing the UnoGame.
 * 
 * @author Rahul and Rishabh
 * @version Apr 24, 2014
 * @author Period: 4
 * @author Assignment: UnoCardGame
 * 
 * @author Sources: Ourselves
 */
public class UnoGame {

	private final static Color[] COLORLIST = { Color.RED, Color.YELLOW,
			Color.GREEN, Color.BLUE }; // Array of possible colors for cards

	private final static int CARD_WIDTH = 43; // Width of card in image

	private final static int CARD_HEIGHT = 65; // Height of card in image

	private final static int CARD_HEIGHT_POS = 64; // Vertical Position of card
													// in image

	private ArrayList<UnoPlayer> playaz; // Array list of players

	private Queue<UnoCard> deck; // Queue representing the deck

	private Stack<UnoCard> discard; // Stack representing discard pile

	private int currentPlayer; // index in ArrayList of the current Player

	private boolean forward; // the order of play - changes with reverse card

	private boolean skip; // true if card on discard pile is a skip

	private boolean difficult; // true if smartAI is used, false if dumbAI

	public int hand; // number of cards per hand

	/**
	 * Constructor. Creates an UnoGame object.
	 * 
	 * @param numPlayers
	 *            is the number of players
	 * @param cardsPerHand
	 *            is the number of cards in the players' hand.
	 * @param names
	 *            is an array of player names
	 */
	public UnoGame(int numPlayers, int cardsPerHand) {
		hand = cardsPerHand;
		deck = new LinkedList<UnoCard>();
		initializeDeck();
		playaz = new ArrayList<UnoPlayer>();
		discard = new Stack<UnoCard>();
		forward = true;
		skip = false;
		playaz.add(new UnoPlayer(this, cardsPerHand));
		currentPlayer = 0;
		discard.push(drawCard());
		while (getCenterCard().getNumber() >= 10) {
			deck.add(discard.pop());
			discard.push(drawCard());
		}
	}

	/**
	 * Adds the cards to the deck and shuffles.
	 */
	private void initializeDeck() {
		BufferedImage allCards = null;
		try {
			allCards = ImageIO.read(new File("images/UNO_cards_deck.bmp"));

		} catch (IOException e) {
		}
		for (int i = 0; i < 4; i++) {
			deck.add(new UnoCard(COLORLIST[i], 0, allCards.getSubimage(0, i
					* CARD_HEIGHT_POS, CARD_WIDTH, CARD_HEIGHT)));
		}
		for (int j = 1; j < 13; j++) {
			for (int i = 0; i < 4; i++) {
				deck.add(new UnoCard(COLORLIST[i], j, allCards.getSubimage(j
						* CARD_WIDTH, i * CARD_HEIGHT_POS, CARD_WIDTH,
						CARD_HEIGHT)));
				deck.add(new UnoCard(COLORLIST[i], j, allCards.getSubimage(j
						* CARD_WIDTH, i * CARD_HEIGHT_POS, CARD_WIDTH,
						CARD_HEIGHT)));
			}
		}
		for (int i = 0; i < 4; i++) {
			deck.add(new UnoCard(Color.BLACK, 13, allCards.getSubimage(
					(13 * CARD_WIDTH), 0, CARD_WIDTH, CARD_HEIGHT)));
			deck.add(new UnoCard(Color.BLACK, 14, allCards
					.getSubimage((13 * CARD_WIDTH), CARD_HEIGHT_POS,
							CARD_WIDTH, CARD_HEIGHT)));
		}
		shuffle();
	}

	/**
	 * Determines the next player based on the card played.
	 */
	public void nextPlayer() {
		if (forward) {
			if (skip) {
				currentPlayer += 2;
			} else {
				currentPlayer++;
			}
		} else {
			if (skip) {
				currentPlayer -= 2;
			} else {
				currentPlayer--;
			}
		}
		currentPlayer %= playaz.size();
		if (currentPlayer < 0)
			currentPlayer = playaz.size() + currentPlayer;
		if (currentPlayer > playaz.size())
			currentPlayer -= playaz.size();
	}

	/**
	 * @return the index of the next player
	 */
	public int getNextPlayer() {
		if (forward) {
			return ((currentPlayer + 1) % playaz.size() + playaz.size())
					% playaz.size();
		}
		return (currentPlayer - 1) % playaz.size();
	}

	/**
	 * Adds card to be played to discard stack.
	 * 
	 * @param card
	 *            is the card to be played
	 */
	public void playCard(UnoCard card) {
		discard.push(card);
		if (card.getNumber() == 11) // if reverse
			forward = !forward;
		if (card.getNumber() == 10) // if skip
		{
			skip = true;
		} else {
			skip = false;
		}
		if (card.getNumber() == 12) // If Draw 2
		{
			if (forward) {
				playaz.get(getNextPlayer()).addCard(drawCard());
				playaz.get(getNextPlayer()).addCard(drawCard());
			}
		}
		if (card.getNumber() == 14) // If Draw 4
		{
			if (forward) {
				playaz.get(getNextPlayer()).addCard(drawCard());
				playaz.get(getNextPlayer()).addCard(drawCard());
				playaz.get(getNextPlayer()).addCard(drawCard());
				playaz.get(getNextPlayer()).addCard(drawCard());
			}
		}
	}

	/**
	 * Adds AI based on the difficulty selected on the menu through the JButtons
	 * 
	 * @param intelligent
	 *            true if smartAI is used, false if dumbAI
	 */
	public void setAI(boolean intelligent) {
		difficult = intelligent;
		for (int i = 1; i < 4; i++) {
			if (intelligent) {
				playaz.add(new UnoSmartAI(this, hand));
			} else {
				playaz.add(new UnoDumbAI(this, hand));
			}
		}
	}

	/**
	 * Removes top card from deck.
	 * 
	 * @return card that is drawn.
	 */
	public UnoCard drawCard() {
		skip = false;
		if (deck.isEmpty())
			flipDiscardToDeck();
		return deck.remove();
	}

	/**
	 * Returns the card that is at the top of the discard stack.
	 * 
	 * @return card at the top of the discard stack
	 */
	public UnoCard getCenterCard() {
		return discard.peek();
	}

	/**
	 * Returns array list of UnoPlayer.
	 * 
	 * @return the array list of UnoPlayer
	 */
	public ArrayList<UnoPlayer> getPlayers() {
		return playaz;
	}

	/**
	 * Returns the queue representing the deck.
	 * 
	 * @return the deck
	 */
	public Queue<UnoCard> getDeck() {
		return deck;
	}

	/**
	 * Returns the stack representing the discard pile.
	 * 
	 * @return the discard stack
	 */
	public Stack<UnoCard> getDiscard() {
		return discard;
	}

	/**
	 * Returns variable representing index of current player in the array list
	 * of players.
	 * 
	 * @return current player
	 */
	public int getCurrentIndex() {
		return currentPlayer;
	}

	/**
	 * Returns the player in the array list at the currentIndex.
	 * 
	 * @return current UnoPlayer
	 */
	public UnoPlayer getCurrentPlayer() {
		return playaz.get(currentPlayer);
	}

	/**
	 * Makes the discard pile the deck to draw from if the deck is empty.
	 */
	public void flipDiscardToDeck() {
		UnoCard temp = discard.pop();
		while (!discard.isEmpty()) {
			deck.add(discard.pop());
		}
		shuffle();
		discard.push(temp);

	}

	/**
	 * Returns true if moving forward, false if a reverse has been played,
	 * flipping the order.
	 * 
	 * @return current direction of play
	 */
	public boolean isForward() {
		return forward;
	}

	/**
	 * Shuffles the cards present in the deck.
	 */
	@SuppressWarnings("unchecked")
	public void shuffle() {
		Collections.shuffle((List<UnoCard>) deck);
	}

	/**
	 * returns the player that has no cards left in their hand
	 * 
	 * @return the winning player
	 */
	public UnoPlayer playerWon() {
		for (UnoPlayer playa : playaz) {
			if (playa.getHand().size() == 0) {
				return playa;
			}
		}
		return null;
	}

	/**
	 * @return true if smartAI used.
	 */
	public boolean getDifficulty() {
		return difficult;
	}

	/**
	 * Returns a string with the names of each UnoPlayer.
	 */
	public String toString() {
		String answer = "";
		for (UnoPlayer player : playaz) {
			answer += player.toString() + " ";
		}
		return answer;
	}
}
