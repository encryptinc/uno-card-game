package defaultPackage;

import java.awt.Color;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Dumb AI for the UnoCardGame project. Can be selected by selecting the EASY
 * JButton in the menu.
 * 
 * @author Rahul and Rishabh
 * @version May 27th, 2014
 * @author Period: 4
 * @author Assignment: UnoCardGame
 * 
 * @author Sources: Ourselves
 */
public class UnoDumbAI extends UnoPlayer {

	Color cols[] = new Color[] { Color.RED, Color.BLUE, Color.GREEN,
			Color.YELLOW }; // Array of possible colors for random selection
							// with a wild

	Random rand = new Random(); // Random object

	/**
	 * @param g
	 *            is the passed game
	 * @param cardsPerHand
	 *            cards in each player's hand to begin
	 */
	public UnoDumbAI(UnoGame g, int cardsPerHand) {
		super(g, cardsPerHand);
	}

	/**
	 * Overrides UnoPlayer's makeMove method to define the movement selection
	 * for the dumbAI
	 */
	@Override
	public void makeMove() {
		if (!canPlay()) {
			addCard(getGame().drawCard());
		}
		for (UnoCard card : getHand()) {
			if (getGame().getCenterCard().canMove(card)) {
				playCard(card);
				if (card.getNumber() >= 13) {
					getGame().getCenterCard().setColor(cols[rand.nextInt(4)]);
				}
				break;
			}
		}
		getGame().nextPlayer();
	}
}
