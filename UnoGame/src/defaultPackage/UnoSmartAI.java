package defaultPackage;

import java.awt.Color;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Smart AI for the UnoCardGame project. Can be selected by selecting the EASY
 * JButton in the menu.
 * 
 * @author Rahul and Rishabh
 * @version May 27th, 2014
 * @author Period: 4
 * @author Assignment: UnoCardGame
 * 
 * @author Sources: Ourselves
 */
public class UnoSmartAI extends UnoPlayer {

	Color cols[] = new Color[] { Color.RED, Color.BLUE, Color.GREEN,
			Color.YELLOW }; // Array of UnoCard Colors

	Random rand = new Random(); // random object

	/**
	 * @param g
	 *            is the current UnoGame
	 * @param cardsPerHand
	 *            is the number of cards per hand for this UnoGame
	 */
	public UnoSmartAI(UnoGame g, int cardsPerHand) {
		super(g, cardsPerHand);
	}

	/**
	 * @param c
	 *            is the passed color
	 * @return the total number of cards with the passed color
	 */
	private int getSum(Color c) {
		int sum = 0;
		for (UnoCard card : getHand()) {
			if (card.getColor().equals(c)) {
				sum += card.getNumber();
			}
		}
		return sum;
	}

	/**
	 * Overrides UnoPlayer's makeMove method to define the movement selection
	 * for the smartAI
	 */
	@Override
	public void makeMove() {
		int[] colorsums = new int[4];
		int index = 0;
		if (!canPlay()) {
			addCard(getGame().drawCard());
		}
		UnoCard best = getHand().get(0);
		for (UnoCard card : getHand()) {
			if (getGame().getCenterCard().canMove(best)
					&& card.getNumber() > best.getNumber()) {
				best = card;
			}
		}
		playCard(best);
		if (best.getNumber() > 13) {
			for (int i = 0; i < 4; i++) {
				colorsums[i] = getSum(cols[i]);
			}
			for (int i = 0; i < 4; i++) {
				if (colorsums[i] > index) {
					index = i;
				}
			}
			getGame().getCenterCard().setColor(cols[index]);
		}

		getGame().nextPlayer();
	}
}
