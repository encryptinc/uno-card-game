package defaultPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * Listener for the Play Again JButton at the end of the game. If clicked, it
 * creates another UnoGame.
 * 
 * @author Rahul and Rishabh
 * @version May 27th, 2014
 * @author Period: 4
 * @author Assignment: UnoCardGame
 * 
 * @author Sources: Ourselves
 */
public class AgainListener implements ActionListener {
	UnoDisplay display; // the current UnoDisplay

	UnoGame game; // the current UnoGame

	/**
	 * Constructor. Initializes variables for UnoGame and UnoDisplay.
	 * 
	 * @param g
	 *            is the current UnoGame
	 * @param d
	 *            is the current UnoDisplay
	 */
	public AgainListener(UnoGame g, UnoDisplay d) {
		display = d;
		game = g;
	}

	/**
	 * Creates a new UnoGame.
	 * 
	 * @param arg0
	 *            is the ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		display.setVisible(false);
		UnoGame newGame = new UnoGame(4, 7);
		new UnoDisplay(newGame.getCurrentPlayer());
	}

}
