package defaultPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Listener for the JButton to quit the game after it has finished.
 * 
 * @author Rahul and Rishabh
 * @version May 27th, 2014
 * @author Period: 4
 * @author Assignment: UnoCardGame
 * 
 * @author Sources: Ourselves
 */
public class QuitListener implements ActionListener {
	/**
	 * Closes the game.
	 * 
	 * @param arg0
	 *            is the ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.exit(0);
	}
}
