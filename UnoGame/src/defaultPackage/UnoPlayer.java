package defaultPackage;

import java.util.ArrayList;

/**
 * Class representing an player in the game.
 * 
 * @author Rahul and Rishabh
 * @version Apr 24, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: Ourselves
 */
public class UnoPlayer {
	private ArrayList<UnoCard> hand; // The UnoPlayer's hand

	private UnoGame game; // UnoGame in which the player is participating.

	/**
	 * Constructor for an Uno Player.
	 * 
	 * @param g
	 *            is the UnoGame object
	 * @param cardsPerHand
	 *            is the number of cards in the hand.
	 */
	public UnoPlayer(UnoGame g, int cardsPerHand) {
		game = g;
		hand = new ArrayList<UnoCard>();
		for (int i = 0; i < cardsPerHand; i++) {
			addCard(game.drawCard());
		}
	}

	/**
	 * Determines whether the player can play given the current center card
	 * 
	 * @return whether the player can play or not
	 */
	public boolean canPlay() {
		for (UnoCard card : hand) {
			if (game.getCenterCard().canMove(card)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Plays the selected card.
	 * 
	 * @param card
	 *            is the UnoCard to be played
	 */
	public void playCard(UnoCard card) {
		game.playCard(removeCard(card));
	}

	/**
	 * Adds a card to this hand.
	 * 
	 * @param card
	 *            card to be added to the current hand.
	 */
	public void addCard(UnoCard card) {
		hand.add(card);
	}

	/**
	 * Obtains the card stored at the specified location in the hand. Does not
	 * remove the card from the hand.
	 * 
	 * @param index
	 *            position of card to be accessed.
	 * @return the card of interest, or the null reference if the index is out
	 *         of bounds.
	 */
	public UnoCard getCard(int index) {
		return hand.get(index);
	}

	/**
	 * Obtains the player's hand. Used for testing purposes.
	 * 
	 * @return the player's hand of UnoCard.
	 */
	public ArrayList<UnoCard> getHand() {
		return hand;
	}

	/**
	 * Removes the specified card from the current hand.
	 * 
	 * @param card
	 *            the card to be removed.
	 * @return the card removed from the hand, or null if the card was not
	 *         present in the hand.
	 */
	public UnoCard removeCard(UnoCard card) {
		int index = hand.indexOf(card);
		if (index < 0) {
			return null;
		} else {
			return hand.remove(index);
		}
	}

	/**
	 * Removes the card at the specified index from the hand.
	 * 
	 * @param index
	 *            poisition of the card to be removed.
	 * @return the card removed from the hand, or the null reference if the
	 *         index is out of bounds.
	 */
	public UnoCard removeCard(int index) {
		return hand.remove(index);
	}

	/**
	 * Returns the game that the player is playing
	 * 
	 * @return the game
	 */
	public UnoGame getGame() {
		return game;
	}

	/**
	 * Gives a string representation of the player.
	 * 
	 * @return hand in a string.
	 */
	public String toString() {
		return hand.toString();
	}

	/**
	 * Empty in UnoPlayer class as the human players will make their own moves.
	 * Overridden in AI classes to define their process of making a move.
	 */
	public void makeMove() {
	}
}
