package defaultPackage;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener class to provide button functionality for selecting wild
 * colors
 * 
 * @author Rahul and Rishabh
 * @version May 8, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: none
 */
public class ColorListener implements ActionListener {
	UnoPlayer player;

	UnoDisplay display;

	Color color;

	/**
	 * Constructor for DeckListener
	 * 
	 * @param g
	 *            The Uno game
	 * @param d
	 *            The display for the Uno game
	 */
	public ColorListener(UnoPlayer p, UnoDisplay d, Color c) {
		player = p;
		display = d;
		color = c;
	}

	/**
	 * Sets the color based on the selected color.
	 * 
	 * @param arg0
	 *            is the ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		player.getGame().getCenterCard().setColor(color);
		display.refreshAll(true);
	}
}