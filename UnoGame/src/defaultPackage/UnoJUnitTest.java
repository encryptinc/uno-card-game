package defaultPackage;

import static org.junit.Assert.*;

import java.awt.Color;

import javax.swing.ImageIcon;

import org.junit.Test;

/**
 * JUnit Test for the Uno Final Project. Intended to test each class for
 * accuracy.
 * 
 * @author Rahul and Rishabh
 * @version Apr 24, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: Ourselves
 */
public class UnoJUnitTest {

	/**
	 * Tests the UnoGame class constructor
	 */
	@Test
	public void testUnoGameConstructor() {
		UnoGame g = new UnoGame(4, 4);
		assertTrue(g.getCenterCard().getNumber() < 10);
		assertTrue(g.getPlayers().size() == 1);
	}
	
	/**
	 * Tests the UnoGame class playerWon method
	 */
	@Test
	public void testUnoGamePlayerWon() {
		UnoGame g = new UnoGame(4, 4);
		assertNull(g.playerWon());
		for (int i = g.getCurrentPlayer().getHand().size() - 1; i >= 0; i--)
		{
			g.getCurrentPlayer().removeCard(i);
		}
		assertTrue(g.playerWon() != null);
	}
	
	/**
	 * Tests the UnoGame class setAI method
	 */
	@Test
	public void testUnoGameSetAI() {
		UnoGame g = new UnoGame(4, 4);
		g.setAI(true);
		assertTrue(g.getDifficulty());
	}

	/**
	 * Tests the UnoGame class nextPlayer method
	 */
	@Test
	public void testUnoGameNextPlayer() {
		UnoGame g = new UnoGame(4, 4);
		assertTrue(g.getCurrentIndex() == 0);
		g.playCard(new UnoCard(Color.RED, 1, null));
		g.nextPlayer();
		assertTrue(g.getCurrentIndex() == 0);
		g.playCard(new UnoCard(Color.RED, 10, null));
		g.nextPlayer();
		assertTrue(g.getCurrentIndex() == 0);
		g.playCard(new UnoCard(Color.RED, 11, null));
		g.nextPlayer();
		assertTrue(g.getCurrentIndex() == 0);
	}

	/**
	 * Tests the UnoCard class constructor
	 */
	@Test
	public void testUnoCardConstructor() {
		UnoCard red2 = new UnoCard(Color.RED, 2, null);
		assertTrue(red2.toString().equals("java.awt.Color[r=255,g=0,b=0] 2"));
		assertTrue(red2.getNumber() == 2);
		assertNull(red2.getCardImage());
	}

	/**
	 * Tests the UnoCard canMove method
	 */
	@Test
	public void testUnoCardCanMove() {
		UnoCard red1 = new UnoCard(Color.RED, 1, null);
		UnoCard red2 = new UnoCard(Color.RED, 2, null);
		UnoCard blue1 = new UnoCard(Color.BLUE, 1, null);
		UnoCard wild = new UnoCard(Color.BLACK, 13, null);
		assertTrue(red1.canMove(red2)); // Checks putting a card of the same
										// color on another
		assertTrue(blue1.canMove(red1)); // Checks putting a same number
											// card of a different color
		assertFalse(blue1.canMove(red2));// Checks putting a different
											// number and color card.
		assertTrue(red1.canMove(wild)); // Checks if wild can be played.
	}

	/**
	 * Tests the UnoPlayer class constructor
	 */
	@Test
	public void testUnoPlayerConstructor() {
		UnoPlayer bob = new UnoPlayer(new UnoGame(1, 7), 7);
		assertTrue(bob.getHand().size() == 7);
	}

	/**
	 * Tests the UnoPlayer addCard method
	 */
	@Test
	public void testUnoPlayerAddCard() {
		UnoPlayer bob = new UnoPlayer(new UnoGame(1, 7), 7);
		UnoCard card = new UnoCard(Color.RED, 1, null);
		bob.addCard(card);
		assertTrue(bob.getHand().contains(card));
	}

	/**
	 * Tests the UnoPlayer removeCard method
	 */
	@Test
	public void testUnoPlayerRemoveCard() {
		UnoPlayer bob = new UnoPlayer(new UnoGame(1, 7), 7);
		UnoCard card1 = new UnoCard(Color.RED, 1, null);
		bob.addCard(card1);
		UnoCard card2 = new UnoCard(Color.BLUE, 1, null);
		bob.addCard(card2);
		bob.removeCard(7);
		assertFalse(bob.getHand().contains(card1));
		bob.removeCard(card2);
		assertFalse(bob.getHand().contains(card2));
	}

	/**
	 * Tests the UnoPlayer canMove method
	 */
	@Test
	public void testUnoPlayerCanMove() {
		UnoGame game = new UnoGame(1, 0);
		UnoPlayer bob = game.getCurrentPlayer();
		bob.addCard(new UnoCard(Color.RED, 1, null));
		bob.addCard(new UnoCard(Color.BLUE, 1, null));
		bob.addCard(new UnoCard(Color.GREEN, 1, null));
		game.playCard(new UnoCard(Color.YELLOW, 2, null));
		assertFalse(bob.canPlay());
		game.playCard(new UnoCard(Color.GREEN, 2, null));
		assertTrue(bob.canPlay());
		game.playCard(new UnoCard(Color.YELLOW, 1, null));
		assertTrue(bob.canPlay());
	}
}
