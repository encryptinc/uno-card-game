package defaultPackage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class for the GUI for the UnoCardGame Final Project. It utilizes a
 * BorderLayout in a JFrame and multiple JPanels to display the cards, deck, and
 * discard pile.
 * 
 * @author Rahul and Rishabh
 * @version May 27th, 2014
 * @author Period: 4
 * @author Assignment: UnoCardGame
 * 
 * @author Sources: Ourselves
 */
public class UnoDisplay extends JFrame {

	private UnoGame game;// The instance of the Uno Game

	private UnoPlayer player; // The display's player

	private JPanel high = new JPanel(); // The top JPanel

	private JPanel low = new JPanel(); // card JPanel

	private JPanel left = new JPanel(); // The left JPanel

	private JPanel right = new JPanel(); // The right JPanel

	private JPanel colorPanel = new JPanel(); // Panel for color select buttons

	private JPanel deckPanel = new JPanel(); // Panel for showing the deck

	private JButton deck = new JButton(); // The button for drawing a card from
											// the deck

	private JButton red = new JButton("RED"); // Button to set color to red

	private JButton blue = new JButton("BLUE"); // Button to set color to blue

	private JButton green = new JButton("GREEN"); // Button to set color to
													// green

	private JButton yellow = new JButton("YELLOW"); // Button to set color to
													// yellow

	private MouseListener hand; // Listener for clicking on a card to play

	private ActionListener draw; // Listener for clicking on the deck

	private ArrayList<UnoPlayer> others = new ArrayList<UnoPlayer>(); // Array
																		// of
																		// other
																		// players

	private JButton playAgain = new JButton("PLAY AGAIN"); // Button to play
															// again

	private JButton leave = new JButton("QUIT"); // Button to quit game at end

	private JPanel finished = new JPanel(); // Panel with quit and play again
											// buttons

	private JButton dumb = new JButton("EASY"); // Option for dumbAI at start of
												// game

	private JButton smart = new JButton("HARD"); // Option for smartAI at start
													// of game

	private JPanel menuPanel = new JPanel(); // Panel with dumb and smart
												// buttons

	/**
	 * Constructor. Sets layouts, adds Icon, initializes variables, adds all
	 * components to panels, sets proper dimensions for components. Opens menu
	 * to choose difficulty.
	 * 
	 * @param p
	 *            is the human player on the current machine.
	 */
	public UnoDisplay(UnoPlayer p) {
		super();

		player = p;
		game = player.getGame();

		hand = new HandListener(player, this);
		draw = new DeckListener(player, this);

		getContentPane().setLayout(new BorderLayout());
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(new ImageIcon("images/Uno_Main_Image.jpg").getImage());
		
		FlowLayout mid = new FlowLayout(FlowLayout.CENTER, 0, (int) Toolkit
				.getDefaultToolkit().getScreenSize().getHeight() / 3);
		colorPanel.setLayout(mid);
		finished.setLayout(mid);
		menuPanel.setLayout(mid);
		deckPanel.setLayout(mid);

		FlowLayout bottom = new FlowLayout(FlowLayout.LEFT, 0, 0);
		low.setLayout(bottom);
		VerticalFlowLayout sides = new VerticalFlowLayout(FlowLayout.LEFT, 0, 0);
		right.setLayout(sides);
		left.setLayout(sides);
		FlowLayout top = new FlowLayout(FlowLayout.CENTER, 0, 0);
		high.setLayout(top);

		deck.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		deck.setBorderPainted(false);
		deck.setContentAreaFilled(false);
		deck.setFocusPainted(false);
		deck.addActionListener(draw);
		try {
			deck.setIcon(new ImageIcon(ImageIO
					.read(new File("images/Deck.jpg"))));
		} catch (IOException e) {
		}

		red.addActionListener(new ColorListener(player, this, Color.RED));
		green.addActionListener(new ColorListener(player, this, Color.GREEN));
		blue.addActionListener(new ColorListener(player, this, Color.BLUE));
		yellow.addActionListener(new ColorListener(player, this, Color.YELLOW));
		red.setBackground(Color.RED);
		red.setPreferredSize(new Dimension(100, 40));
		blue.setBackground(Color.CYAN);
		blue.setPreferredSize(new Dimension(100, 40));
		green.setBackground(Color.GREEN);
		green.setPreferredSize(new Dimension(100, 40));
		yellow.setBackground(Color.YELLOW);
		yellow.setPreferredSize(new Dimension(100, 40));
		colorPanel.add(red);
		colorPanel.add(blue);
		colorPanel.add(green);
		colorPanel.add(yellow);

		playAgain.setPreferredSize(new Dimension(150, 40));
		leave.setPreferredSize(new Dimension(150, 40));
		finished.add(playAgain);
		finished.add(leave);
		playAgain.addActionListener(new AgainListener(game, this));
		leave.addActionListener(new QuitListener());

		dumb.setPreferredSize(new Dimension(150, 40));
		smart.setPreferredSize(new Dimension(150, 40));
		menuPanel.add(dumb);
		menuPanel.add(smart);
		dumb.addActionListener(new DifficultyListener(dumb, game, this));
		smart.addActionListener(new DifficultyListener(smart, game, this));

		menu();
		setVisible(true);

	}

	/**
	 * Sets the menu screen to select the difficulty for the AI.
	 */
	public void menu() {
		add(menuPanel, BorderLayout.CENTER);
	}

	/**
	 * Refreshes the panel for the top player (player 3).
	 * 
	 * @param player
	 *            is the UnoPlayer on the top portion of the screen (player 3)
	 */
	private void setTop(UnoPlayer player) {
		high.removeAll();
		for (UnoCard card : player.getHand())
			high.add(new JLabel(new ImageIcon("images/Top Player Cards.jpg")));
		add(high, BorderLayout.PAGE_START);
	}

	/**
	 * Refreshes the panel for the left player (player 2).
	 * 
	 * @param player
	 *            is the UnoPlayer on the left portion of the screen (player 2)
	 */
	private void setLeft(UnoPlayer player) {
		left.removeAll();
		for (UnoCard card : player.getHand())
			left.add(new JLabel(new ImageIcon("images/VerticalDeck.jpg")));
		add(left, BorderLayout.LINE_START);
	}

	/**
	 * Refreshes the panel for the right player (player 4).
	 * 
	 * @param player
	 *            is the UnoPlayer on the right portion of the screen (player 4)
	 */
	private void setRight(UnoPlayer player) {
		right.removeAll();
		for (UnoCard card : player.getHand())
			right.add(new JLabel(new ImageIcon("images/VerticalDeckRight.jpg")));
		add(right, BorderLayout.LINE_END);
	}

	/**
	 * Refreshes the panel with the deck and discard pile to reflect each move.
	 * Removes panel and replaces with a 4 button panel to choose the next color
	 * when you play a wild card.
	 * 
	 * @param showdeck
	 *            determines whether a wild has been played.
	 */
	private void setMiddle(boolean showdeck) {
		remove(menuPanel);
		deckPanel.removeAll();
		if (showdeck) {
			remove(colorPanel);
			deckPanel.removeAll();
			deckPanel.add(new JLabel(new ImageIcon(game.getCenterCard()
					.getCardImage())));
			deckPanel.add(deck);
			makeTitle();
			add(deckPanel, BorderLayout.CENTER);
		} else {
			remove(deckPanel);
			add(colorPanel, BorderLayout.CENTER);
		}
	}

	/**
	 * Sets the title of the JFrame based on the current color.
	 */
	private void makeTitle() {
		String color = "";
		Color col = game.getCenterCard().getColor();
		if (col.equals(Color.RED)) {
			color = "Red";
		} else if (col.equals(Color.BLUE)) {
			color = "Blue";
		} else if (col.equals(Color.GREEN)) {
			color = "Green";
		} else if (col.equals(Color.YELLOW)) {
			color = "Yellow";
		}
		setTitle("The current color is " + color);
	}

	/**
	 * Refreshes the panel for the bottom player (player 1) or the human player
	 * playing the game.
	 * 
	 * @param clickable
	 *            indicates whether it is this player's turn.
	 */
	private void setBottom(boolean clickable) {
		low.removeAll();

		low.removeMouseListener(hand);
		for (UnoCard card : player.getHand()) {
			low.add(new JLabel(new ImageIcon(card.getCardImage())));
		}
		if (clickable) {
			low.addMouseListener(hand);
		}
		add(low, BorderLayout.PAGE_END);
	}

	/**
	 * Sets a JTextField when someone has won and adds options to play again or
	 * quit.
	 * 
	 * @param winner
	 *            is the UnoPlayer who has won the game
	 */
	private void setEnd(UnoPlayer winner) {
		this.remove(left);
		this.remove(right);
		this.remove(high);
		this.remove(low);
		this.remove(colorPanel);
		this.remove(deckPanel);
		String win;
		if (winner == player) {
			win = "You win";
		} else {
			win = "You lose";
		}
		setTitle(win);
		add(finished, BorderLayout.CENTER);

	}

	/**
	 * Refreshes each of the individual panels based on the move made by the
	 * players. Also calls method to modify title based on color as well as
	 * setting the game winning menu once a player has won.
	 * 
	 * @param showAndClick
	 *            is a boolean variable passed to the setMiddle and setBottom
	 *            methods.
	 */
	public void refreshAll(boolean showAndClick) {
		makeTitle();
		others.clear();
		for (UnoPlayer play : game.getPlayers()) {
			if (play != player) {
				others.add(play);
			}
		}
		if (game.getCenterCard().getColor() != Color.BLACK)
			while (game.getCurrentPlayer() != player) {
				game.getCurrentPlayer().makeMove();
			}
		setLeft(others.get(0));
		setTop(others.get(1));
		setRight(others.get(2));
		setMiddle(showAndClick);
		setBottom(showAndClick);
		if (game.playerWon() != null) {
			setEnd(game.playerWon());
		}
		revalidate();
		repaint();
		setVisible(true);
	}

	/**
	 * Main method. Creates a new UnoGame and UnoDisplay object.
	 * 
	 * @param args
	 *            is a passed string array
	 */
	public static void main(String[] args) {
		UnoGame game = new UnoGame(4, 7);
		new UnoDisplay(game.getCurrentPlayer());
	}
}
