package defaultPackage;

import java.awt.Color;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

/**
 * Card for an Uno game.
 * 
 * @author Rishabh and Rahul
 * @version Apr 23, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: John K. Estell
 */
public class UnoCard {
	private Color col; // color of the card to check for canPlay.

	private int cardType; // number or type of card. 0-9 = numbers. 10-12 =
							// special
							// cards. 13 = wild, 14 = draw 4 wild

	private BufferedImage face; // image file for the card

	/**
	 * Constructor. Creates an uno card.
	 * 
	 * @param type
	 *            is the color of the card
	 * @param num
	 *            is the number or type of card
	 * @param bufferedImage
	 *            is the image file for the card
	 */
	public UnoCard(Color type, int num, BufferedImage bufferedImage) {
		col = type;
		cardType = num;
		face = bufferedImage;
	}

	/**
	 * Determines whether the passed card can be played.
	 * 
	 * @param card
	 *            is the card in the players' hand.
	 * @return whether the card can be played
	 */
	public boolean canMove(UnoCard card) {
		return card.getColor().equals(col) || card.getNumber() == cardType
				|| card.getNumber() >= 13;

	}

	/**
	 * Return the variable col.
	 * 
	 * @return color of the card.
	 */
	public Color getColor() {
		return col;
	}

	/**
	 * Sets a new color for wild cards
	 * 
	 * @param c
	 *            new color for card
	 */
	public void setColor(Color c) {
		col = c;
	}

	/**
	 * Returns the variable cardType.
	 * 
	 * @return the number or type of the card.
	 */
	public int getNumber() {
		return cardType;
	}

	/**
	 * Returns the graphic image of the card.
	 * 
	 * @return an icon containing the graphic image of the card.
	 */
	public BufferedImage getCardImage() {
		return face;
	}

	/**
	 * Returns a string representation of the card.
	 */
	public String toString() {
		return col + " " + cardType;
	}
}
