package defaultPackage;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * HandListener class to provide playing cards functionality
 * 
 * @author Rahul and Rishabh
 * @version May 8, 2014
 * @author Period: 4
 * @author Assignment: UnoGame
 * 
 * @author Sources: none
 */
public class HandListener implements MouseListener {

	UnoPlayer player; // Human UnoPlayer

	UnoDisplay display; // current UnoDisplay

	/**
	 * Constructor. Initializes variables for human UnoPlayer and UnoDisplay
	 * 
	 * @param p
	 *            is the human UnoPlayer
	 * @param d
	 *            is the UnoDisplay
	 */
	public HandListener(UnoPlayer p, UnoDisplay d) {
		player = p;
		display = d;
	}

	/**
	 * Plays the selected card.
	 * 
	 * @param e
	 *            is the MouseEvent
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (player.getGame().getCurrentPlayer() == player) {
			UnoCard card;
			if (e.getX() / 43 < player.getHand().size()) {
				card = player.getCard(e.getX() / 43);
				if (player.getGame().getCenterCard().canMove(card)) {
					player.playCard(card);
					player.getGame().nextPlayer();
				}
				display.refreshAll(card.getNumber() < 13);

			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

}
