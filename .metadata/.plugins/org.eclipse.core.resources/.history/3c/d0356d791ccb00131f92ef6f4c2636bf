package defaultPackage;

import java.awt.Color;

public class CardType
{
    private Color col;
    private int number; 
    
    /**
     * The suit clubs.
     */
    public final static CardType red = new CardType( "Red",  );
    /**
     * The suit diamonds.
     */
    public final static CardType blue = new CardType( "Blue", );
    /**
     * The suit hearts.
     */
    public final static CardType green = new CardType( "Green", "c" );
    /**
     * The suit spades.
     */
    public final static CardType yellow = new CardType( "Yellow", "c" );
    
    
   /**
    * List of all suit values.  Primarily for use with iteration.
    */
    public final static java.util.List VALUES = 
       Collections.unmodifiableList( 
          Arrays.asList( new Suit[] { CLUBS, DIAMONDS, HEARTS, SPADES } ) );
    
    
    // Constructor - declared private as only the predefined values should
    // be used by the client.
    private Suit( String nameValue, String symbolValue ) {
       name = nameValue;
       symbol = symbolValue;
    }
        

   /**
    *  Returns a description of this suit.
    *  @return the name of the suit.
    */
    public String getName() {
        return name;
    }
  
  
  /**
    *  The symbol associated with this suit.  Returns the symbol, which
    *  usually constitutes a single character, in the form of a string.
    *  Symbol is used for the construction of the filenames of the card images.
    *  @return string containing the symbol for the suit.
    */ 
    public String getSymbol() {
       return symbol;
    }
     
     
   /**
    * Returns a description of this suit.
    * @return the name of this suit.
    */
    public String toString() {
       return name;
    }
    
    
   /** 
    *  Compares the suits.  Used for the purpose of sorting cards in a hand or deck.
    *  @param otherSuitObject the other suit.
    *  @return < 0 if this suit is lower than the other suit, 0 if the suits
    *  are the same, or > 0 if this suit is higher than the other suit.
    */   
    public int compareTo( Object otherSuitObject ) {
       Suit otherSuit = (Suit) otherSuitObject;
       return VALUES.indexOf( this ) - VALUES.indexOf( otherSuit );
    } 
     
 }

     
}
