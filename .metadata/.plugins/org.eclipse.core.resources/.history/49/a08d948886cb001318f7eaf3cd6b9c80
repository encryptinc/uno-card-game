package defaultPackage;

import java.util.ArrayList;


public class UnoHand
{
    private ArrayList<UnoCard> hand = new ArrayList<UnoCard>();

    private UnoGame game;


    public UnoHand( UnoGame g, int cardsPerHand )
    {
        game = g;
        for ( int i = 0; i < cardsPerHand; i++ )
        {
            addCard( game.drawCard() );
        }
    }


    /**
     * Adds a card to this hand.
     * 
     * @param card
     *            card to be added to the current hand.
     */
    public void addCard( UnoCard card )
    {
        hand.add( card );
    }


    /**
     * Obtains the card stored at the specified location in the hand. Does not
     * remove the card from the hand.
     * 
     * @param index
     *            position of card to be accessed.
     * @return the card of interest, or the null reference if the index is out
     *         of bounds.
     */
    public UnoCard getCard( int index )
    {
        return (UnoCard)hand.get( index );
    }

    /**
     * Removes the specified card from the current hand.
     * 
     * @param card
     *            the card to be removed.
     * @return the card removed from the hand, or null if the card was not
     *         present in the hand.
     */
    public UnoCard removeCard( UnoCard card )
    {
        int index = hand.indexOf( card );
        if ( index < 0 )
            return null;
        else
            return (UnoCard)hand.remove( index );
    }


    /**
     * Removes the card at the specified index from the hand.
     * 
     * @param index
     *            poisition of the card to be removed.
     * @return the card removed from the hand, or the null reference if the
     *         index is out of bounds.
     */
    public UnoCard removeCard( int index )
    {
        return (UnoCard)hand.remove( index );
    }


    public String toString()
    {
        String answer = "";
        for ( UnoCard card : hand )
        {
            answer += card.toString() + " ";
        }
        return answer;
    }
}
